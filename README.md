Prova - Java JR - v.1.2.pdf
---------------------------------------------------------------------
Respostas para a prova de "Software Developer Java JR".

Questão 1
----------------------------------------------------------------
    Resposta: 
        e) Nenhuma das Alternativas acima.

    Justificativa:
        Após executar "Thread t = new Thread(new Test());", o metodo "Runnable" do objeto chamado, portanto, executando "run()";
        Saída correta do Código:

            "Executou o Final do Metodo.
            Qual é a musica?
            java.lang.RuntimeException: Opa, temos um problema!"

Questão 2
----------------------------------------------------------------
    Resposta:
        b) Código compila e lança um erro “StackOverFlowError”

    Justificativa:
        Esta linha do código "TextInput input = new NumericInput();" esta dando NEW na classe NumericInput() diversas vezes, por isto o erro "“StackOverFlowError”

Questão 3
----------------------------------------------------------------
    Resposta:
     h) throws java.lang.ClassCastException

    Justificativa
        Está ocorrendo a conversão de um inteiro em uma String.

    Possível solução:
        public class ListExample {
        
            @SuppressWarnings("unchecked")
            public static void main(String[] args) {
                
                @SuppressWarnings("rawtypes")
                List lst = new ArrayList();
                lst.add(new String("12 "));
                lst.add(new String("Opaa "));
                lst.add(new String("true "));
                
                Arrays.sort(lst.toArray());
                
                for (int i = 0; i < lst.size(); i++) {
                    System.out.print(lst.get(i).toString());
                }
                
            }
        }
        Saída: 12 Opaa true 

Questão 4
----------------------------------------------------------------
    Resposta:
        c) public static void main(String...[] a){

    Justificativa:
        A JVM inicia o programa Java, chamando o metodo 'main'

    Questão 5

        Resposta:
            d) Man Father Son

Questão 6
----------------------------------------------------------------
    Resposta:
        a) protected long age(int x,int y) { return 0; }


Questão 7
----------------------------------------------------------------
    Resposta:
        a) Qualquer Classe;

    Justificativa:
        Uma classe com modificar de acesso 'public', pode ser acessada ir qualquer entidade que consiga visualizar a classe a que ela pertence.

Questão 8
----------------------------------------------------------------
    Resposta:
        d) c;

Questão 9
----------------------------------------------------------------
    Resposta:
        b) true false

Questão 10
----------------------------------------------------------------
    Resposta:
        b) 179

Questão 11
----------------------------------------------------------------
    Resposta:
        c) 2

